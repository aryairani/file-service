package fileservice;

public class Sha256Id {
    final String id;

    public Sha256Id(String id) {
        this.id = id;
        assert(id.length() == 64);
    }

    public String prefix() {
        return id.substring(0,2);
    }

    public String suffix() {
        return id.substring(2);
    }

    @Override
    public String toString() {
        return id;
    }

    static public Sha256Id from(String id) {
        return new Sha256Id(id);
    }
}
