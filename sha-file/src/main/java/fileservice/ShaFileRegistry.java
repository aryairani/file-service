package fileservice;

import org.apache.commons.codec.binary.Hex;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ShaFileRegistry implements Registry2<Sha256Id> {
    final Path root;

    public ShaFileRegistry(Path root) {
        this.root = root;
    }

    @Override
    public Result<Sha256Id> save(InputStream is) {
        try {
            MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            File tempFile = File.createTempFile("DiskFileService", "received");
            FileOutputStream fos = new FileOutputStream(tempFile);

            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while (bytesRead != -1) {
                bytesRead = is.read(buffer);
                if (bytesRead > 0) {
                    sha256.update(buffer, 0, bytesRead);
                    fos.write(buffer, 0, bytesRead);
                }
            }
            fos.close();
            is.close();

            Sha256Id id = new Sha256Id(Hex.encodeHexString(sha256.digest()));

            Path destination = pathFor(id);

            Files.createDirectories(destination.getParent());
            Files.deleteIfExists(destination);
            Files.move(tempFile.toPath(), destination);

            return Result.success(id);

        } catch (NoSuchAlgorithmException e) {
            return Result.failure(e);
        } catch (IOException e) {
            return Result.failure(e);
        }
    }

    public Path pathFor(Sha256Id id) {
        return root.resolve(id.prefix()).resolve(id.suffix());
    }

    @Override
    public Result<InputStream> load(Sha256Id id) {
        try {
            return Result.success(new FileInputStream(pathFor(id).toFile()));
        } catch (FileNotFoundException e) {
            return Result.failure(new RuntimeException(e));
        }
    }

    @Override
    public Result<Boolean> delete(Sha256Id id) {
        try {
            return Result.success(Files.deleteIfExists(pathFor(id)));
        } catch (IOException e) {
            return Result.failure(e);
        }
    }

}
