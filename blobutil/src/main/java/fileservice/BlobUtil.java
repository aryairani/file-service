package fileservice;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public class BlobUtil {
    private final static Path defaultRepo = Paths.get("/tmp", "blobutil_repo");

    private static void printUsage() {
        System.err.println("Usage: ");
        System.err.println("  BlobUtil save        - save file from stdin");
        System.err.println("  BlobUtil load <id>   - copy file <id> to stdout");
        System.err.println("  BlobUtil delete <id> - delete file <id>");
        System.err.println();
        System.err.println("Default repo location of " + defaultRepo + " by setting environment variable BLOBUTIL_REPO");
    }

    private static void validateBlobId(String s) {
        if (!Pattern.compile("^\\p{XDigit}{64}$").matcher(s).matches()) {
            System.err.println("A blob id must be 64 hex digits");
            System.exit(1);
        }
    }

    public static void main(String[] args) throws IOException {
        Path root = Optional.ofNullable(System.getenv("BLOBUTIL_REPO"))
                .map(s -> Paths.get(s))
                .orElseGet(() -> defaultRepo);

        Registry2<Sha256Id> service2 = new ShaFileRegistry(root);
        Registry<Sha256Id> service = Registry.fromRegistry2(service2);

        if (args.length == 0) {
            printUsage();
        } else switch(args[0].toLowerCase()) {
            case "save":
                Sha256Id saveId = service.save(System.in);
                System.err.println("saved blob: " + saveId);
                break;
            case "load":
                if (args.length < 2) printUsage();
                else {
                    validateBlobId(args[1]);
                    Sha256Id loadId = Sha256Id.from(args[1]);

                    Consumer<InputStream> doCopy = stream -> {
                        try {
                            IOUtils.copy(stream, System.out);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    };
                    Consumer<Throwable> showError = error -> {
                        System.err.println("error loading blob: " + loadId);
                        System.err.println("The error was: " + error.getMessage());
                    };
                    service2.load(loadId).to(doCopy, showError);
                }
                break;
            case "delete":
                if (args.length < 2) printUsage();
                else {
                    validateBlobId(args[1]);
                    Sha256Id deleteId = Sha256Id.from(args[1]);
                    if (!service.delete(deleteId))
                        System.err.println("blob not found: " + deleteId);
                    else System.err.println("deleted blob: " + deleteId);
                }
                break;
            default:
                printUsage();
        }
    }
}
