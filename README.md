# Simple File Service

```xml
<dependency>
    <groupId>org.gtri</groupId>
    <artifactId>stream-registry-core</artifactId>
    <version>0.1</version>
</dependency>
```

## Basic interface (no error handling):

```java
/** Save and retrieve file blobs by ID token */
public interface Registry<ID> {
    ID save(InputStream is);
    InputStream load(ID id);
    boolean delete(ID id); // false if no match

    // convert between ID types
    default <ID2> Registry<ID2> xmap(Function<? super ID, ? extends ID2> f, Function<? super ID2, ? extends ID> g) { /*...*/ }
    // downgrade from error capture to no error handling, because exceptions are the spice of life
    static <ID> Registry<ID> fromRegistry2(Registry2<ID> underlying) { /*...*/ }
}
```

## Error-handling interfaces:

Two alternative interfaces are provided along with static methods to convert to a reduced level of error-handling if desired.

```java
/** preserves errors */
public interface Registry2<ID> {
    Result<ID> save(InputStream is);
    Result<InputStream> load(ID id);
    Result<Boolean> delete(ID id);

    // convert between ID types
    default <ID2> Registry2<ID2> xmap(Function<? super ID, ? extends ID2> f, Function<? super ID2, ? extends ID> g) { /*...*/ } // convert between ID types    
}

/** discards errors */
public interface Registry1<ID> {
    Optional<ID> save(InputStream is);
    Optional<InputStream> load(ID id);
    Optional<Boolean> delete(ID id);

    // convert between ID types
    default <ID2> Registry1<ID2> xmap(Function<? super ID, ? extends ID2> f, Function<? super ID2, ? extends ID> g) { /*...*/ } // convert between ID types    
    // downgrade from error capture to error discarding, because we just want to know if worked or not.
    static <ID> Registry1<ID> fromRegistry2(Registry2<ID> underlying) { /*...*/ }
}
```

## Example disk-based instantiation:

```xml
<dependency>
    <groupId>org.gtri</groupId>
    <artifactId>stream-registry-sha-file</artifactId>
    <version>0.1</version>
</dependency>
```

```java
/** Save and retrieve blobs from a repository on disk */
public class DiskShaFileService2 implements FileService2<Sha256Id> {
  public DiskShaFileService(Path repositoryRoot) { /* ... */ }
  public Result<Sha256Id> save(InputStream is) { /* ... */ }
  public Result<InputStream> load(Sha256Id id) { /* ... */ }
  public Result<boolean> delete(Sha256Id id) { /* ... */ }
}
```

where `Sha256Id` is a wrapper around a `String`:

```java
public class Sha256Id {
  final String id;
  public String prefix() { return id.substring(0,2); }
  public String suffix() { return id.substring(2); }
}
```

## Example Amazon S3 implementation:

```xml
<dependency>
    <groupId>org.gtri</groupId>
    <artifactId>stream-registry-s3</artifactId>
    <version>0.1</version>
</dependency>
```

```java
public class S3UuidRegistry implements Registry2<UUID> {
    public S3UuidRegistry(AmazonS3 s3, String bucketName)  { /*...*/ }
    public Result<UUID> save(InputStream is) { /*...*/ }
    public Result<InputStream> load(UUID uuid) { /*...*/ }
    public Result<Boolean> delete(UUID uuid) { /*...*/ }
}
```

## Example usage:

```scala
import java.io.StringReader
import java.nio.file.Paths

import org.apache.commons.io.IOUtils
import org.apache.commons.io.input.ReaderInputStream

import fileservice._

object Test extends App {
  val root = Paths.get("/tmp","fileservice-tests")
  val service: FileService[Sha256Id] = new DiskShaFileService(root)

  val inputData = "hello!"
  def streamToSave = new ReaderInputStream(new StringReader(inputData))

  val id = service.save(streamToSave)
  println(s"saved stream, got id $id")

  val loadedStream = service.load(id);
  println(s"loading stream    id $id")

  val data = IOUtils.toString(loadedStream)
  println(s"got data: $data")
}
```

yields:
```text
saved stream, got id ce06092fb948d9ffac7d1a376e404b26b7575bcc11ee05a4615fef4fec3a308b
loading stream    id ce06092fb948d9ffac7d1a376e404b26b7575bcc11ee05a4615fef4fec3a308b
got data: hello!
```

## Sample command-line client:
```text
$ java -jar BlobUtil.jar
Usage: 
  BlobUtil save        - save file from stdin
  BlobUtil load <id>   - copy file <id> to stdout
  BlobUtil delete <id> - delete file <id>

Default repo location of /tmp/blobutil_repo by setting environment variable BLOBUTIL_REPO

$ echo 'hello, world!' | BLOBUTIL_REPO=demo java -jar BlobUtil.jar save
saved blob: 4dca0fd5f424a31b03ab807cbae77eb32bf2d089eed1cee154b3afed458de0dc

$ BLOBUTIL_REPO=demo java -jar BlobUtil.jar save < ../build.sbt 
saved blob: 0483b1b8584778d344c3f855d2612787bb51bf52b093ecb1913d75c53c2df4e3

$ BLOBUTIL_REPO=demo java -jar BlobUtil.jar load 4dca0fd5f424a31b03ab807cbae77eb32bf2d089eed1cee154b3afed458de0dc
hello, world!

$ BLOBUTIL_REPO=demo java -jar BlobUtil.jar load 0483b1b8584778d344c3f855d2612787bb51bf52b093ecb1913d75c53c2df4e3 | head -n 1
name := "file-api"

$ find demo -type f
demo/04/83b1b8584778d344c3f855d2612787bb51bf52b093ecb1913d75c53c2df4e3
demo/4d/ca0fd5f424a31b03ab807cbae77eb32bf2d089eed1cee154b3afed458de0dc

$ BLOBUTIL_REPO=demo java -jar BlobUtil.jar delete 0483b1b8584778d344c3f855d2612787bb51bf52b093ecb1913d75c53c2df4e3
deleted blob: 0483b1b8584778d344c3f855d2612787bb51bf52b093ecb1913d75c53c2df4e3

$ BLOBUTIL_REPO=demo java -jar BlobUtil.jar delete 0483b1b8584778d344c3f855d2612787bb51bf52b093ecb1913d75c53c2df4e3
blob not found: 0483b1b8584778d344c3f855d2612787bb51bf52b093ecb1913d75c53c2df4e3

$ find demo -type f
demo/4d/ca0fd5f424a31b03ab807cbae77eb32bf2d089eed1cee154b3afed458de0dc
```
