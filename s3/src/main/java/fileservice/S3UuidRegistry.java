package fileservice;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;

import java.io.InputStream;
import java.util.UUID;

public class S3UuidRegistry implements Registry2<UUID> {
    private final AmazonS3 s3;
    final String bucketName;

    public S3UuidRegistry(AmazonS3 s3, String bucketName) {
        this.s3 = s3;
        this.bucketName = bucketName;
    }

    @Override
    public Result<UUID> save(InputStream is) {
        UUID uuid = UUID.randomUUID();
        try {
            s3.putObject(bucketName, uuid.toString(), is, new ObjectMetadata());
            return Result.success(uuid);
        } catch (Exception e) {
            return Result.failure(e);
        }
    }

    @Override
    public Result<InputStream> load(UUID uuid) {
        try {
            return Result.success(s3.getObject(new GetObjectRequest(bucketName, uuid.toString())).getObjectContent());
        } catch (Exception e) {
            return Result.failure(e);
        }
    }

    @Override
    public Result<Boolean> delete(UUID uuid) {
        try {
            boolean doesExist = s3.doesObjectExist(bucketName, uuid.toString());
            s3.deleteObject(bucketName, uuid.toString());
            return Result.success(doesExist);
        } catch (Exception e) {
            return Result.failure(e);
        }
    }
}
