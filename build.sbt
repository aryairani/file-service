import sbt.Keys.{autoScalaLibrary, libraryDependencies}

def moduleName(m: String) = name := s"stream-registry-$m"

lazy val root = project.in(file(".")).aggregate(core, `sha-file`, s3, blobutil)

lazy val core = project.settings(commonSettings)

lazy val `sha-file` = project
  .settings(commonSettings)
  .dependsOn(core)
  .settings(
    moduleName("disk-sha1"),
    libraryDependencies += "commons-io" % "commons-io" % "2.4",
    libraryDependencies += "commons-codec" % "commons-codec" % "1.10"
  )

lazy val s3 = project
  .settings(commonSettings)
  .dependsOn(core)
  .settings(
    moduleName("aws"),
    // https://mvnrepository.com/artifact/com.amazonaws/aws-java-sdk-s3
    libraryDependencies += "com.amazonaws" % "aws-java-sdk-s3" % "1.11.15"
  )

lazy val blobutil = project
  .settings(commonSettings)
  .dependsOn(`sha-file`)
  .settings(
    moduleName("blob-util"),
    com.github.retronym.SbtOneJar.oneJarSettings,
    artifactName in oneJar := ( (_, _, _) => "BlobUtil.jar" )
  )

lazy val commonSettings = Seq(
  organization := "org.gtri",
  javacOptions in (Compile, compile) += "-Xlint:unchecked",
  version := "0.1",
  crossPaths := false,
  autoScalaLibrary := false
) ++ publishSettings

lazy val publishSettings = Seq(
  publishMavenStyle := true,
  publishTo := {
    val s = if (isSnapshot.value) "snapshot" else "release"
    Some(s"arfam-ci-${s}s" at s"https://arfam-ci.gtri.gatech.edu/artifactory/libs-$s-local")
  }
)

