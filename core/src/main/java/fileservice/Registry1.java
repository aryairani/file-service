package fileservice;

import java.io.InputStream;
import java.util.Optional;
import java.util.function.Function;

public interface Registry1<ID> {
    Optional<ID> save(InputStream is);
    Optional<InputStream> load(ID id);
    Optional<Boolean> delete(ID id);

    default <ID2> Registry1<ID2> xmap(Function<? super ID, ? extends ID2> f, Function<? super ID2, ? extends ID> g) {
        assert Registry1.this != this;
        return new Registry1<ID2>() {
            public Optional<ID2> save(InputStream is) { return Registry1.this.save(is).map(f); }
            public Optional<InputStream> load(ID2 id2) { return Registry1.this.load(g.apply(id2)); }
            public Optional<Boolean> delete(ID2 id2) { return Registry1.this.delete(g.apply(id2)); }
        };
    }

    static <ID> Registry1<ID> fromRegistry2(Registry2<ID> underlying) {
        return new Registry1<ID>() {
            public Optional<ID> save(InputStream is) { return underlying.save(is).success; }
            public Optional<InputStream> load(ID id) { return underlying.load(id).success; }
            public Optional<Boolean> delete(ID id) { return underlying.delete(id).success; }
        };
    }
}