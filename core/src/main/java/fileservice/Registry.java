package fileservice;

import java.io.InputStream;
import java.util.function.Function;

/** This Registry throws RuntimeExceptions on errors.
 * See [[Registry1]] or [[Registry2]] for ones that don't. */
interface Registry<ID> {

    ID save(InputStream is);

    InputStream load(ID id);

    /** returns false if ID not found */
    boolean delete(ID id);

    default <ID2> Registry<ID2> xmap(Function<? super ID, ? extends ID2> f, Function<? super ID2, ? extends ID> g) {
        assert Registry.this != this;
        return new Registry<ID2>() {
            public ID2 save(InputStream is) { return f.apply(Registry.this.save(is)); }
            public InputStream load(ID2 id2) { return Registry.this.load(g.apply(id2)); }
            public boolean delete(ID2 id2) { return Registry.this.delete(g.apply(id2)); }
        };
    }

    static <ID> Registry<ID> fromRegistry2(Registry2<ID> underlying) {
        return new Registry<ID>() {
            public ID save(InputStream is) { return underlying.save(is).orElseThrow(); }
            public InputStream load(ID id) { return underlying.load(id).orElseThrow(); }
            public boolean delete(ID id) { return underlying.delete(id).orElseThrow(); }
        };
    }
}