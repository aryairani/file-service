package fileservice;

import java.io.InputStream;
import java.util.function.Function;

public interface Registry2<ID> {
    Result<ID> save(InputStream is);
    Result<InputStream> load(ID id);
    Result<Boolean> delete(ID id);
    default <ID2> Registry2<ID2> xmap(Function<? super ID, ? extends ID2> f, Function<? super ID2, ? extends ID> g) {
        assert Registry2.this != this;
        return new Registry2<ID2>() {
            public Result<ID2> save(InputStream is) { return Registry2.this.save(is).map(f); }
            public Result<InputStream> load(ID2 id2) { return Registry2.this.load(g.apply(id2)); }
            public Result<Boolean> delete(ID2 id2) { return Registry2.this.delete(g.apply(id2)); }
        };
    }
}