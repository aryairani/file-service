package fileservice;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class Result<A> {
    public final Optional<A> success;
    public final Optional<Throwable> failure;

    private Result(Optional<A> success, Optional<Throwable> failure) {
        this.success = success;
        this.failure = failure;
    }

    public static <A> Result<A> success(A a) {
        return new Result<>(Optional.of(a), Optional.empty());
    }

    public static <A> Result<A> failure(Throwable t) {
        return new Result<>(Optional.empty(), Optional.of(t));
    }

    public boolean isSuccess() {
        return success.isPresent();
    }

    public boolean isFailure() {
        return failure.isPresent();
    }

    public Result<A> orElse(Result<A> that) {
        return isSuccess() ? this : that;
    }

    public A orElseThrow() {
        try {
            return success.orElseThrow(failure::get);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public <B> Result<B> map(Function<? super A, ? extends B> f) {
        return new Result<>(success.map(f), failure);
    }

    public <B> Result<B> flatMap(Function<? super A, ? extends Result<B>> f) {
        return success.map(f).orElseGet(() -> new Result<>(Optional.empty(), failure));
    }

    public <B> B bimap(Function<? super A, ? extends B> onSuccess, Function<? super Throwable, ? extends B> onFailure) {
        return success.map(onSuccess).orElseGet(failure.map(onFailure)::get);
    }

    public void to(Consumer<? super A> onSuccess, Consumer<? super Throwable> onFailure) {
        if (success.isPresent())
            success.ifPresent(onSuccess);
        else failure.ifPresent(onFailure);
    }

    public Optional<A> toOptional() {
        return success;
    }
}
